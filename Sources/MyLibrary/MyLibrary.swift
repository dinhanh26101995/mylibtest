import UIKit
public struct MyLibrary {
  public private(set) var text = "Hello, World!"
  
  public init() {
  }
  
  public func getViewController() -> UIViewController {
    return MyViewController(nibName: "MyViewController", bundle: nil)
  }
}
